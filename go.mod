module github.com/boris1993/dnsupdater

go 1.12

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.1
	github.com/sirupsen/logrus v1.2.0
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/sys v0.0.0-20181206074257-70b957f3b65e
	gopkg.in/yaml.v2 v2.2.2
)
